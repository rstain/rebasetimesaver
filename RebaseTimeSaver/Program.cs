﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace RebaseTimeSaver
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length >= 2 && args[0] == "save")
            {
                SaveImpl.Save(args[1]);
                return;
            }

            if (args.Length >= 2 && args[0] == "restore")
            {
                bool dryRun = args.Contains("-n") || args.Contains("--dry-run");
                RestoreImpl.Restore(args[1], dryRun);
                return;
            }

            if (args.Length >= 2 && args[0] == "git")
            {
                string[] gitArgs = new string[args.Length - 1];
                Array.Copy(args, 1, gitArgs, 0, gitArgs.Length);
                GitCommandImpl.Invoke(gitArgs);
                return;
            }

            PrintUsage();
        }

        static void PrintUsage()
        {
            // Getting executable file name: http://stackoverflow.com/questions/616584/how-do-i-get-the-name-of-the-current-executable-in-c

            Console.Out.WriteLine(
                "{0} save <working_dir>\n" +
                "    Saves timestamps.\n\n" +
                "{0} restore <working_dir>\n" +
                "    Restores timestamps saved by the \"save\" command.\n\n" +
                System.Diagnostics.Process.GetCurrentProcess().ProcessName);
        }
    }

    class SaveImpl: Impl
    {
        public static void Save(string workingDir)
        {
            string repoRoot = GetRepositoryRoot(workingDir);
            if (string.IsNullOrEmpty(repoRoot))
            {
                Console.Out.WriteLine("!!! Not a git repository?");
                return;
            }

            string gitDir = GetGitDir(repoRoot);
            string outputFile = Path.Combine(gitDir, "timestamp.txt");
            using (FileStream stream = new FileStream(outputFile, FileMode.Create))
            using (StreamWriter writer = new StreamWriter(stream))
            {
                List<string> files = GitFileList(repoRoot);
                files.Sort();
                foreach (string escapedFileName in files)
                {
                    string fileRel = UnescapeFileDisplayName(escapedFileName);
                    string fileAbs = Path.Combine(repoRoot, fileRel);

                    // Если поддиректория является подмодулем (submodule), она также присутствует в списке. Для поддиректорий
                    // не надо считать хэш.
                    try
                    {
                        if ((File.GetAttributes(fileAbs) & FileAttributes.Directory) == FileAttributes.Directory)
                            continue;
                    }
                    catch (FileNotFoundException)
                    {
                        // Нормальная ситуация - файл удален в результате rebase
                        continue;
                    }
                    catch (DirectoryNotFoundException)
                    {
                        // Нормальная ситуация - директория удалена в результате rebase
                        continue;
                    }

                    DateTime lastWriteTimeUtc = File.GetLastWriteTimeUtc(fileAbs);
                    byte[] hash = CalcHash(fileAbs);
                    writer.WriteLine(fileRel + "|" + lastWriteTimeUtc.ToBinary() + "|" + lastWriteTimeUtc.ToString() + "|" + FormatHashAsString(hash));
                }
            }
        }
    }

    class RestoreImpl: Impl
    {
        public static void Restore(string workingDir, bool dryRun)
        {
            string repoRoot = GetRepositoryRoot(workingDir);
            if (string.IsNullOrEmpty(repoRoot))
            {
                Console.Out.WriteLine("!!! Not a git repository?");
                return;
            }

            List<String> fileList = GitFileList(repoRoot);
            fileList.Sort();
            string gitDir = GetGitDir(repoRoot);
            string timestampsFile = Path.Combine(gitDir, "timestamp.txt");
            List<FileData> stored = ParseTimestampFile(timestampsFile);
            stored.Sort();
            List<FileData> toRestore = GetDataToRestore(repoRoot, fileList, stored);
            if (!dryRun)
                RestoreTimestamp(repoRoot, toRestore);
            DumpData(toRestore);
        }

        static List<FileData> ParseTimestampFile(string fileName)
        {
            List<FileData> result = new List<FileData>();

            using (var reader = new StreamReader(fileName))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    String[] parts = line.Split('|');
                    FileData fd = new FileData();
                    fd.fileName = parts[0];
                    fd.timeStamp = long.Parse(parts[1]);
                    fd.hash = StringToByteArray(parts[3]);
                    result.Add(fd);
                }
            }
            return result;
        }

        // Возвращает файлы, время модификации которых, необходимо восстановить.
        // fileList - список файлов в репозитории.
        // storedData - данные, сохраненные ранее командой Save.
        static List<FileData> GetDataToRestore(string repoRoot, List<String> fileList, List<FileData> storedData)
        {
            List<FileData> result = new List<FileData>();

            int fileIndex = 0;
            int dataIndex = 0;
            while (fileIndex < fileList.Count() && dataIndex < storedData.Count())
            {
                string escapedFileName = fileList[fileIndex];
                string fileName = UnescapeFileDisplayName(escapedFileName);

                int compareResult = String.Compare(fileName, storedData[dataIndex].fileName, true);
                if (compareResult < 0)
                {
                    // Новый файл - ничего с ним не делаем
                    ++fileIndex;
                    continue;
                }
                if (compareResult > 0)
                {
                    // Файл удален - ничего не делаем
                    ++dataIndex;
                    continue;
                }
                DateTime timeNow = File.GetLastWriteTimeUtc(Path.Combine(repoRoot, fileName));
                DateTime timeBefore = DateTime.FromBinary(storedData[dataIndex].timeStamp);
                if (timeNow == timeBefore)
                {
                    ++fileIndex;
                    ++dataIndex;
                    continue;
                }

                byte[] hashNow = CalcHash(Path.Combine(repoRoot, fileName));
                if (ByteArraysEqual(hashNow, storedData[dataIndex].hash))
                    result.Add(storedData[dataIndex]);

                ++fileIndex;
                ++dataIndex;
            }
            return result;
        }

        // Воосстанавливает время модификации файлов.
        static void RestoreTimestamp(string repoRoot, List<FileData> data)
        {
            foreach (FileData fd in data)
            {
                DateTime dt = DateTime.FromBinary(fd.timeStamp);
                File.SetLastWriteTimeUtc(Path.Combine(repoRoot, fd.fileName), dt);
            }
        }

        // Выводит в стандартынй вывод список файлов (которые были или должны быть восстановлены).
        static void DumpData(List<FileData> data)
        {
            foreach (FileData fd in data)
            {
                Console.WriteLine(fd.fileName);
            }
        }
    }

    class GitCommandImpl : Impl
    {
        public static void Invoke(string[] args)
        {
            string workingDir = Directory.GetCurrentDirectory();
            string repoRoot = GetRepositoryRoot(workingDir);
            if (string.IsNullOrEmpty(repoRoot))
            {
                Console.Out.WriteLine("!!! Not a git repository?");
                return;
            }

            string gitDir = GetGitDir(repoRoot);
            Logger logger = new Logger();
            logger.Open(Path.Combine(gitDir, "RebaseTimeSaver.log"));
            logger.Log(String.Format("Started the Invoke command: {0}", String.Join(" ", args)));

            SaveImpl.Save(workingDir);
            logger.Log("Timestamps saved");

            List<string> stdOut = null;
            List<string> stdErr = null;
            if (!Git(String.Join(" ", args), workingDir, ref stdOut, ref stdErr))
            {
                foreach (string s in stdOut)
                    Console.WriteLine("{0}", s);
                TextWriter errorWriter = Console.Error;
                foreach (string s in stdErr)
                    errorWriter.WriteLine("{0}", s);
                Environment.ExitCode = 1;
            }
            logger.Log("Git command completed, restoring timestamps");
            RestoreImpl.Restore(repoRoot, false);
            logger.Log("Timestamps restored");
        }
    }

    class Impl
    {
        public static string GetRepositoryRoot(string workingDir)
        {
            List<string> stdOut = null;
            List<string> stdErr = null;
            Git("rev-parse --show-toplevel", workingDir, ref stdOut, ref stdErr);
            return stdOut.Count == 0 ? "" : stdOut[0];
        }

        // Возвращает .git директорию с учетом того, является ли репозиторий модулем или сабмодулем
        public static string GetGitDir(string repoRoot)
        {
            if (IsSubmodule(repoRoot))
                return Path.Combine(repoRoot, GetSubmoduleGitDir(repoRoot));
            else
                return Path.Combine(repoRoot, ".git");
        }

        static bool IsSubmodule(string repoRoot)
        {
            return (File.GetAttributes(Path.Combine(repoRoot, ".git")) & FileAttributes.Directory) != FileAttributes.Directory;
        }

        // Возвращает рабочую директорию родительского модуля для сабмодуля с заданной рабочей директорией.
        // Возвращаемый путь - относительно submoduleRoot.
        static string GetSubmoduleGitDir(string submoduleRoot)
        {
            string gitFilePath = Path.Combine(submoduleRoot, ".git");
            foreach (string str in File.ReadAllLines(gitFilePath))
            {
                // Первая строка имеет вид:
                // gitdir: ../.git/modules/SubmoduleName
                string[] components = str.Split(' ');
                return components[1];
            }

            throw new Exception(string.Format("The file {0} is empty", gitFilePath));
        }

        public static List<string> GitFileList(string repoRoot)
        {
            List<string> stdOut = null;
            List<string> stdErr = null;
            Git("ls-files", repoRoot, ref stdOut, ref stdErr);
            return stdOut;
        }

        // Конвертирует имя файла, возвращаемое командой git ls-files, в валидное имя файла.
        //
        // Если имя файла содержит не ASCI символы, команды git (по крайней мере ls-files) кодирует их
        // в последовательности групп \NNN,
        // NNN - это восьмеричное представление байта из UTF-8 кодировки символа.
        // Например, символ Й кодируется в UTF-8 двумя байтами: D099. В имени файла он будет представлен как \320\231.
        // (Квотинг можно отключить через параметр core.quotepath.)
        //
        public static string UnescapeFileDisplayName(string displayFileName)
        {
            // Алгоритм ниже - это автомат с двумя состояниями.
            // 1. Копирование. Копируем текущий символ и переходим к следующему.
            // 2. Накапливание UTF-8 символов.

            StringBuilder builder= new StringBuilder(displayFileName.Length);
            List<byte> utf8_buffer = new List<byte>();
            bool accumulating = false;

            for (int i = 0; i < displayFileName.Length; )
            {
                if (displayFileName[i] == '\\')
                {
                    if (!accumulating)
                        accumulating = true;

                    string numStr = displayFileName.Substring(i + 1, 3);
                    byte b = Convert.ToByte(numStr, 8);
                    utf8_buffer.Add(b);

                    i += 4;
                }
                else
                {
                    if (accumulating)
                    {
                        byte[] inBuf = new byte[utf8_buffer.Count];
                        utf8_buffer.CopyTo(inBuf);
                        string converted = ConvertUtf8Buffer(inBuf, utf8_buffer.Count);
                        builder.Append(converted);

                        accumulating = false;
                        utf8_buffer.Clear();
                    }

                    builder.Append(displayFileName[i]);

                    ++i;
                }
            }

            if (accumulating)
            {
                byte[] inBuf = new byte[utf8_buffer.Count];
                utf8_buffer.CopyTo(inBuf);
                string converted = ConvertUtf8Buffer(inBuf, utf8_buffer.Count);
                builder.Append(converted);

                accumulating = false;
                utf8_buffer.Clear();
            }

            // Если имя файла содержит пробел, гит заключает его в кавычки. Удаляем их.
            if (builder[0] == '"')
                builder.Remove(0, 1);
            if (builder.Length != 0 && builder[builder.Length - 1] == '"')
                builder.Remove(builder.Length - 1, 1);

            return builder.ToString();
        }

        static string ConvertUtf8Buffer(byte[] buffer, int bufferSize)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            Decoder decoder = encoding.GetDecoder();

            int outBufSize = encoding.GetMaxCharCount(bufferSize);
            char[] outBuf = new char[outBufSize];

            bool completed = false;
            int bytesUsed;
            int charsUsed;

            decoder.Convert(buffer, 0, bufferSize, outBuf, 0, outBufSize,
                false, out bytesUsed, out charsUsed, out completed);

            return new string(outBuf, 0, charsUsed);
        }

        public static bool Git(string arguments, string workingDir, ref List<string> stdOut, ref List<string> stdErr)
        {
            return ExecProcess("git.exe", arguments, workingDir, ref stdOut, ref stdErr) == 0;
        }

        // Стартует процесс, дожидается его завершения и возвращает содержимое стандартного потока вывода.
        static int ExecProcess(string fileName, string arguments, string workingDir, ref List<string> stdOut, ref List<string> stdErr)
        {
            // Запуск процесса и получение его stdout - http://stackoverflow.com/questions/4291912/process-start-how-to-get-the-output
            var proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = fileName,
                    Arguments = arguments,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = true,
                    WorkingDirectory = workingDir,
                }
            };

            // Обработчик асинхронного чтения стандартного вывода процесса
            List<string> localStdOut = new List<string>();
            stdOut = localStdOut;
            proc.OutputDataReceived += delegate(object sendingProcess, DataReceivedEventArgs outLine)
            {
                // В конце передается null. Не выяснял, так для всех процессов или это специфично для git.exe.
                if (outLine.Data != null)
                    localStdOut.Add(outLine.Data);
            };

            proc.Start();

            // Do not perform a synchronous read to the end of both redirected streams.
            // string output = p.StandardOutput.ReadToEnd();
            // string error = p.StandardError.ReadToEnd();
            // p.WaitForExit();
            // Use asynchronous read operations on at least one of the streams.
            //
            proc.BeginOutputReadLine();
            stdErr = new List<string>();
            while (!proc.StandardError.EndOfStream)
                stdErr.Add(proc.StandardError.ReadLine());
            proc.WaitForExit();

            return proc.ExitCode;
        }

        // Вычисляет хэш заданного файла.
        public static byte[] CalcHash(String fileName)
        {
            // Подсчет SHA-1: http://stackoverflow.com/questions/1993903/how-do-i-do-a-sha1-file-checksum-in-c
            // Там же есть ссылка на топик, где обсуждается использование BufferedStream (не вникал).
            //
            const int maxTryCount = 3;
            int tryCount = 0;
            for (; ; )
            {
                try
                {
                    using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                    using (BufferedStream bs = new BufferedStream(fs))
                    {
                        using (SHA1Managed sha1 = new SHA1Managed())
                        {
                            return sha1.ComputeHash(bs);
                        }
                    }
                }
                catch (IOException)
                {
                    if (tryCount == maxTryCount - 1)
                        throw;
                    ++tryCount;
                    Thread.Sleep(500);
                }
            }
        }

        public static byte[] StringToByteArray(String hex)
        {
            // http://stackoverflow.com/questions/311165/how-do-you-convert-byte-array-to-hexadecimal-string-and-vice-versa

            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        // Эта функция работает в любой версии .Net.
        // Более элегантные/быстрые варианты - см. http://stackoverflow.com/questions/43289/comparing-two-byte-arrays-in-net
        public static bool ByteArraysEqual(byte[] a1, byte[] a2)
        {
            if (a1.Length != a2.Length)
                return false;

            for (int i = 0; i < a1.Length; i++)
                if (a1[i] != a2[i])
                    return false;

            return true;
        }

        // Форматирует хэш в виде строки шестнадцатеричной строки
        public static string FormatHashAsString(byte[] hash)
        {
            return FormatBytes(hash);
        }

        // Форматирует массив байтов в виде шестнадцатеричной строки.
        static string FormatBytes(byte[] hash)
        {
            // http://stackoverflow.com/questions/1993903/how-do-i-do-a-sha1-file-checksum-in-c
            // http://stackoverflow.com/questions/311165/how-do-you-convert-byte-array-to-hexadecimal-string-and-vice-versa

            StringBuilder formatted = new StringBuilder(2 * hash.Length);
            foreach (byte b in hash)
            {
                formatted.AppendFormat("{0:X2}", b);
            }
            return formatted.ToString();
        }

        public struct FileData: IComparable<FileData>
        {
            public String fileName;
            public long timeStamp;
            public byte[] hash;

            public int CompareTo(FileData other)
            {
                int result = 0;
                result = fileName.CompareTo(other.fileName);
                if (result != 0)
                    return result;
                return 0;
            }
        }
    }
}
