﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RebaseTimeSaver
{
    class Logger
    {
        public void Open(string fileName)
        {
            writer_ = new StreamWriter(fileName, true);
            FileInfo fi = new FileInfo(fileName);
            if (fi.Length != 0)
                writer_.WriteLine();
        }

        public void Log(string text)
        {
            writer_.WriteLine("{0:s} {1}", DateTime.Now, text);
            writer_.Flush();
        }

        StreamWriter writer_;
    }
}
