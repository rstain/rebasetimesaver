Утилита RebaseTimeSaver.exe позволяет сократить количество пересобираемых файлов при выполнении ребэйза.
Она позволяет сохранять время изменения всех файлов в репозитории и восстанавливать его у тех файлов, содержимое которых не изменилось.

Как пользоваться
	Настройка - см. ниже.
	Команда Save File Times сохраняет время изменения всех файлов в репозитории. Команда Restore File Times восстанавливает.
	
	Тут я хотел написать сценарии использования, но пока лень... ;)

Как настроить
	Скопировать файл RebaseTimeSaver.exe в любую директорию.
	Добавить в SmartGit следующие команды (меню Edit|Preferences, вкладка Tools):

		Menu Item Name: Rebase Interactive
		Command: путь к RebaseTimeSaver.exe
		Arguments: git rebase -i ${commit}
		Handles: SHAs
		Включить галку Show output and wait until finished

		Menu Item Name: Save File Times
		Command: путь к RebaseTimeSaver.exe
		Arguments: save ${repositoryRootPath}
		Handles: N/A
		Включить галку Show output and wait until finished

		Menu Item Name: Restore File Times
		Command: путь к RebaseTimeSaver.exe
		Arguments: restore ${repositoryRootPath}
		Handles: N/A
		Включить галку Show output and wait until finished
